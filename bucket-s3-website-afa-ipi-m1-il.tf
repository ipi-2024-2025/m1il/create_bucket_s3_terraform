resource "aws_s3_bucket_website_configuration" "ipi-website" {
  depends_on = [
    aws_s3_object.file_upload_index_html,
    aws_s3_object.file_upload_error_html
  ]
  bucket = aws_s3_bucket.afa-ipi-M1-IL.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}