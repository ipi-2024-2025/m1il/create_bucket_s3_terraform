output "website_url" {
  value       = "http://${aws_s3_bucket_website_configuration.ipi-website.website_endpoint}"
  description = "URL of the website"
}
