# Terraform bucket S3 and host website

Ce code Terraform permet de hoster un website sur AWS via S3

## Requirements

- Installer Terraform sur sa machine (idéalement sur une Linux)
- Installer awscli
- Avoir un compte AWS

### Créer des clefs API - Dans IAM (sur la console AWS)

- Créer un user
- Donner des droits (soit par un groupe soit directement dessus)
- Dans la section User > Security Credentials > Access Key

### Dans le terminal (sur votre poste / VM distante / container)

- Lancer la commande `aws configure`
- Renseigner les clefs d'accès précédentes
- Renseigner la région et le format (json)

### Lancement de Terraform

- terraform fmt
- terraform init
- terraform validate
- terraform plan
- terraform apply (-auto-approve si on est sûr de nous)