resource "aws_s3_object" "file_upload_index_html" {
  bucket       = resource.aws_s3_bucket.afa-ipi-M1-IL.bucket
  key          = "index.html"
  content      = file("index.html")
  content_type = "text/html"
}

resource "aws_s3_object" "file_upload_error_html" {
  bucket       = resource.aws_s3_bucket.afa-ipi-M1-IL.bucket
  key          = "error.html"
  content      = file("error.html")
  content_type = "text/html"
}