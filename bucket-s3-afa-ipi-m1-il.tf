resource "aws_s3_bucket" "afa-ipi-M1-IL" {
  bucket = "afa-ipi-m1-il"

  tags = {
    Name        = "afa-ipi-M1-IL"
    Environment = "Dev"
    Purpose     = "Training"
  }
}

resource "aws_s3_bucket_public_access_block" "s3-acl-public-access-block" {
  bucket = aws_s3_bucket.afa-ipi-M1-IL.bucket

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_policy" "allow_access_from_internet" {
  depends_on = [aws_s3_bucket_public_access_block.s3-acl-public-access-block]
  bucket     = aws_s3_bucket.afa-ipi-M1-IL.bucket
  policy     = file("access_from_internet.json")
}